# coding: utf-8

# :nodoc:
module CelluloidDBus
  autoload :Connection, 'celluloid_dbus/connection'
  autoload :SystemBus,  'celluloid_dbus/system_bus'
end
