# coding: utf-8

# :nodoc:
module CoworkConcierge
  #
  # The superior man understands the transitory in the light of the eternity of
  # the end.
  #
  VERSION = '0.0.0'
end
