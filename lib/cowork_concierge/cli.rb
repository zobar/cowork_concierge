# coding: utf-8

require 'bluez'
require 'celluloid'
require 'pp'

# :nodoc:
module CoworkConcierge::Cli
  include Celluloid
  include Celluloid::Notifications

  def cleanup
    adapter.discoverable = @old_discoverable
  end

  def initialize(*args)
    @args = args
    async.start
  end

  private

  def adapter
    manager.default_adapter
  end

  def manager
    BlueZ.manager
  end

  def start
    @old_discoverable, adapter.discoverable = adapter.discoverable, true
  end
end
