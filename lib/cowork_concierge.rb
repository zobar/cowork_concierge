# coding: utf-8

#
# Opens the damn door. It's better because it's more complicated.
#
module CoworkConcierge
  autoload :Agent,   'cowork_concierge/agent'
  autoload :Cli,     'cowork_concierge/cli'
  autoload :VERSION, 'cowork_concierge/version'
end
