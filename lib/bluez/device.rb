# coding: utf-8

# :nodoc:
module BlueZ::Device
  include BlueZ::Interface

  def self.notification(registry, member, args)
    case member
    when 'DisconnectRequested' then yield 'disconnect_requested', *args
    when 'PropertyChanged'     then yield 'device_property_changed', *args
    end
  end

  #
  # @!attribute [r] adapter
  #
  # The object path of the adapter the device belongs to.
  #
  # @return [Adapter]
  #
  def adapter
    registry[get_device_properties['Adapter']]
  end

  #
  # @!attribute alias
  #
  # The name alias for the remote device. The alias can be used to have a
  # different friendly name for the remote device.
  #
  # In case no alias is set, it will return the remote device name. Setting an
  # empty string as alias will convert it back to the remote device name.
  #
  # When reseting the alias with an empty string, the emitted PropertyChanged
  # signal will show the remote name again.
  #
  # @return [String]
  #
  def alias
    get_device_properties['Alias']
  end

  def alias=(value)
    set_device_property 'Alias', value
  end

  def bdaddr
    @bdaddr ||= device_address
      .match(/(\h\h):(\h\h):(\h\h):(\h\h):(\h\h):(\h\h)/)
      .captures
      .reverse
      .map { |octet| octet.to_i 16 }
      .pack('c*')
  end

  #
  # @!attribute blocked
  #
  # If set to true any incoming connections from the device will be immediately
  # rejected. Any device drivers will also be removed and no new ones will be
  # probed as long as the device is blocked.
  #
  def blocked
    get_device_properties['Blocked']
  end

  def blocked=(value)
    set_device_property 'Blocked', value
  end

  #
  # @!attribute [r] connected
  #
  # Indicates if the remote device is currently connected. A PropertyChanged
  # signal indicate changes to this status.
  #
  # @return [Boolean]
  #
  def connected
    get_device_properties['Connected']
  end

  def device
    @device ||= proxy['org.bluez.Device']
  end

  #
  # @!attribute [r] device_address
  #
  # The Bluetooth device address of the remote device.
  #
  # @return [String]
  #
  def device_address
    get_device_properties['Address']
  end

  #
  # @!attribute [r] device_class
  #
  # The Bluetooth class of device of the remote device.
  #
  # @return [Fixnum]
  #
  def device_class
    get_device_properties['Class']
  end

  #
  # @!attribute [r] device_name
  #
  # The Bluetooth remote name. This value can not be changed. Use the Alias
  # property instead.
  #
  # @return [String]
  #
  def device_name
    get_device_properties['Name']
  end

  #
  # @!attribute [r] device_uuids
  #
  # List of 128-bit UUIDs that represents the available remote services.
  #
  # @return [Array<String>]
  #
  def device_uuids
    get_device_properties['UUIDs']
  end

  #
  # @!attribute [r] icon
  #
  # Proposed icon name according to the freedesktop.org icon naming
  # specification.
  #
  # @return [String]
  #
  def icon
    get_device_properties['Icon']
  end

  #
  # @!attribute [r] legacy_pairing
  #
  # Set to true if the device only supports the pre-2.1 pairing mechanism. This
  # property is useful in the Adapter.DeviceFound signal to anticipate whether
  # legacy or simple pairing will occur.
  #
  # Note that this property can exhibit false-positives in the case of
  # Bluetooth 2.1 (or newer) devices that have disabled Extended Inquiry
  # Response support.
  #
  # @return [Boolean]
  #
  def legacy_pairing
    get_device_properties['LegacyPairing']
  end

  #
  # @!attribute [r] paired
  #
  # Indicates if the remote device is paired.
  #
  # @return [Boolean]
  #
  def paired
    get_device_properties['Paired']
  end

  #
  # @!attribute [r] product
  #
  # Product unique numeric identifier.
  #
  # @return [Fixnum]
  #
  def product
    get_device_properties['Product']
  end

  #
  # @!attribute [r] services
  #
  # List of characteristics based services.
  #
  # @return [Array<String>]
  #
  def services
    get_device_properties['Services']
  end

  #
  # @!attribute trusted
  #
  # Indicates if the remote is seen as trusted. This setting can be changed by
  # the application.
  #
  # @return [Boolean]
  #
  def trusted
    get_device_properties['Trusted']
  end

  def trusted=(value)
    set_device_property 'Trusted', value
  end

  #
  # @!attribute [r] vendor
  #
  # Vendor unique numeric identifier.
  #
  # @return [Fixnum]
  #
  def vendor
    get_device_properties['Vendor']
  end

  #
  # @!attribute [r] version
  #
  # Version unique numeric identifier.
  #
  # @return [Fixnum]
  #
  def version
    get_device_properties['Version']
  end

  #
  # This method will cancel any previous DiscoverServices transaction.
  #
  # @return [void]
  #
  def cancel_discovery
    device.CancelDiscovery
  end

  #
  # This method disconnects a specific remote device by terminating the
  # low-level ACL connection. The use of this method should be restricted to
  # administrator use.
  #
  # A DisconnectRequested signal will be sent and the actual disconnection will
  # only happen 2 seconds later. This enables upper-level applications to
  # terminate their connections gracefully before the ACL connection is
  # terminated.
  #
  # @return [void]
  #
  def disconnect
    device.Disconnect
  end

  #
  # This method starts the service discovery to retrieve remote service
  # records. The pattern parameter can be used to specify specific UUIDs. And
  # empty string will look for the public browse group.
  #
  # The return value is a dictionary with the record handles as keys and the
  # service record in XML format as values. The key is uint32 and the value a
  # string for this dictionary.
  #
  # @param pattern [String]
  # @return [Hash{String => String}]
  #
  def discover_services(pattern)
    device.DiscoverServices(pattern).first
  end

  #
  # Returns all properties for the device. See the properties section for
  # available properties.
  #
  # @return [Hash{String => Object}]
  #
  def get_device_properties
    device.GetProperties.first
  end

  #
  # Changes the value of the specified property. Only properties that are
  # listed a read-write are changeable.
  #
  # On success this will emit a PropertyChanged signal.
  #
  # @param name [String]
  # @param value [Object]
  # @return [void]
  #
  def set_device_property(name, value)
    device.SetProperty name, value
  end

  def sockaddr_l2(psm = 0, cid = 0)
    [AF_BLUETOOTH, psm, bdaddr, cid].pack 'SS<a6S<'
  end
end
