# coding: utf-8

require 'celluloid_dbus'

module BlueZ
  # :nodoc:
  class Registry
    include Celluloid
    include Celluloid::Notifications

    class << self
      def interfaces
        {
          'org.bluez.Adapter' => Adapter,
          'org.bluez.Device'  => Device,
          'org.bluez.Manager' => Manager
        }
      end
    end

    def [](path)
      cache[path] ||= build path
    end

    def dbus_signal(_, path, interface, member, *args)
      interface = self.class.interfaces[interface]
      unless interface.nil?
        source = self[path]
        interface.notification self, member, args do |topic, *interface_args|
          publish topic, source, *interface_args
        end
      end
    end

    def initialize
      super
      subscribe 'dbus_signal', :dbus_signal
    end

    private

    def bluez
      @bluez ||= bus.service 'org.bluez'
    end

    def build(path)
      proxy = bluez.object(path).tap(&:introspect)
      interfaces = self.class.interfaces.values_at(*proxy.interfaces).compact
      Object.new self, proxy, interfaces
    end

    def bus
      @bus ||= CelluloidDBus::SystemBus.new
    end

    def cache
      @cache ||= {}
    end
  end
end
