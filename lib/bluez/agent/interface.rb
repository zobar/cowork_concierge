# coding: utf-8

require 'dbus'

# :nodoc:
class BlueZ::Agent::Interface < DBus::Object
  attr_reader :agent, :bus

  def initialize(bus, agent)
    path = "/ruby/#{Process.pid}/#{agent.class}/#{agent.object_id.to_s(16)}"
      .tr '^/0-9A-Z_a-z', '_'
    super path
    @agent = agent
    @bus = bus
  end

  dbus_interface 'org.bluez.Agent' do
    dbus_method :Authorize, 'in device:o, in uuid:s' do |device, uuid|
      agent.authorize bus[device], uuid
    end

    dbus_method(:Cancel) { agent.cancel }

    dbus_method :ConfirmModeChange, 'in mode:s' do
      agent.confirm_mode_change
    end

    dbus_method(
      :DisplayPasskey,
      'in device:o, in passkey:u, in entered:y') do |device, passkey, entered|
      agent.display_passkey bus[device], passkey, entered
    end

    dbus_method(:Release) { agent.release }

    dbus_method :RequestPinCode, 'in device:o, out result:s' do |device|
      [agent.request_pin_code(bus[device])]
    end

    dbus_method :RequestPasskey, 'in device:o, out result:u' do |device|
      [agent.request_passkey(bus[device])]
    end

    dbus_method(
      :RequestConfirmation, 'in device:o, in passkey:u') do |device, passkey|
      agent.request_confirmation bus[device], passkey
    end
  end
end
