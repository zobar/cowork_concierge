# coding: utf-8

# :nodoc:
module BlueZ::Adapter
  include BlueZ::Interface

  def self.notification(registry, member, args)
    case member
    when 'DeviceCreated'     then yield 'device_created', registry[args[0]]
    when 'DeviceDisappeared' then yield 'device_disappeared', *args
    when 'DeviceFound'       then yield 'device_found', *args
    when 'DeviceRemoved'     then yield 'device_removed', registry[args[0]]
    when 'PropertyChanged'   then yield 'adapter_property_changed', *args
    end
  end

  def adapter
    @adapter ||= proxy['org.bluez.Adapter']
  end

  #
  # @!attribute [r] adapter_address
  #
  # The Bluetooth device address.
  #
  # @return [String]
  #
  def adapter_address
    get_adapter_properties['Address']
  end

  #
  # @!attribute [r] adapter_class
  #
  # The Bluetooth class of device.
  #
  # @return [Fixnum]
  #
  def adapter_class
    get_adapter_properties['Class']
  end

  #
  # @!attribute adapter_name
  #
  # The Bluetooth friendly name. This value can be changed and a
  # PropertyChanged signal will be emitted.
  #
  # @return [String]
  #
  def adapter_name
    get_adapter_properties['Name']
  end

  def adapter_name=(value)
    set_adapter_property 'Name', value
  end

  #
  # @!attribute [r] adapter_uuids
  #
  # List of 128-bit UUIDs that represents the available local services.
  #
  # @return [Array<String>]
  #
  def adapter_uuids
    get_adapter_properties['UUIDs']
  end

  #
  # @!attribute [r] devices
  #
  # List of device object paths.
  #
  # @return [Array<Device>]
  #
  def devices
    get_adapter_properties['Devices'].map(&registry.method(:[]))
  end

  #
  # @!attribute discoverable
  #
  # Switch an adapter to discoverable or non-discoverable to either make it
  # visible or hide it. This is a global setting and should only be used by
  # the settings application.
  #
  # If the DiscoverableTimeout is set to a non-zero value then the system will
  # set this value back to false after the timer expired.
  #
  # In case the adapter is switched off, setting this value will fail.
  #
  # When changing the Powered property the new state of this property will be
  # updated via a PropertyChanged signal.
  #
  # @return [Boolean]
  #
  def discoverable
    get_adapter_properties['Discoverable']
  end

  def discoverable=(value)
    set_adapter_property 'Discoverable', value
  end

  #
  # @!attribute discoverable_timeout
  #
  # The discoverable timeout in seconds. A value of zero means that the
  # timeout is disabled and it will stay in discoverable/limited mode forever.
  #
  # The default value for the discoverable timeout should be 180 seconds (3
  # minutes).
  #
  # @return [Fixnum]
  #
  def discoverable_timeout
    get_adapter_properties['DiscoverableTimeout']
  end

  def discoverable_timeout=(value)
    set_adapter_property 'DiscoverableTimeout', value
  end

  #
  # @!attribute [r] discovering
  #
  # Indicates that a device discovery procedure is active.
  #
  # @return [Boolean]
  #
  def discovering
    get_adapter_properties['Discovering']
  end

  #
  # @!attribute pairable
  #
  # Switch an adapter to pairable or non-pairable. This is a global setting
  # and should only be used by the settings application.
  #
  # Note that this property only affects incoming pairing requests.
  #
  # @return [Boolean]
  #
  def pairable
    get_adapter_properties['Pairable']
  end

  def pairable=(value)
    set_adapter_property 'Pairable', value
  end

  #
  # @!attribute pairable_timeout
  #
  # The pairable timeout in seconds. A value of zero means that the timeout is
  # disabled and it will stay in pareable mode forever.
  #
  # @return [Fixnum]
  #
  def pairable_timeout
    get_adapter_properties['PairableTimeout']
  end

  def pairable_timeout=(value)
    set_adapter_property 'PairableTimeout', value
  end

  #
  # @!attribute powered
  #
  # Switch an adapter on or off. This will also set the appropriate
  # connectable state.
  #
  # @return [Boolean]
  #
  def powered
    get_adapter_properties['Powered']
  end

  def powered=(value)
    set_adapter_property 'Powered', value
  end

  #
  # Aborts either a CreateDevice call or a CreatePairedDevice call.
  #
  # @param address [String]
  # @return void
  #
  def cancel_device_creation
    adapter.CancelDeviceCreation
  end

  #
  # Creates a new object path for a remote device. This method will connect to
  # the remote device and retrieve all SDP records.
  #
  # If the object for the remote device already exists this method will fail.
  #
  # @param address [String]
  # @return [Device]
  #
  def create_device(address)
    registry[adapter.CreateDevice(address).first]
  end

  #
  # Creates a new object path for a remote device. This method will connect to
  # the remote device and retrieve all SDP records and then initiate the
  # pairing.
  #
  # If previously CreateDevice was used successfully, this method will only
  # initiate the pairing.
  #
  # Compared to CreateDevice this method will fail if the pairing already
  # exists, but not if the object path already has been created. This allows
  # applications to use CreateDevice first and the if needed use
  # CreatePairedDevice to initiate pairing.
  #
  # The agent object path is assumed to reside within the process (D-Bus
  # connection instance) that calls this method. No separate registration
  # procedure is needed for it and it gets automatically released once the
  # pairing operation is complete.
  #
  # The capability parameter is the same as for the RegisterAgent method.
  #
  # @param address [String]
  # @param agent [Agent]
  # @return [Device]
  #
  def create_paired_device(address, agent, capability)
    agent_path = registry.export agent
    result = adapter.CreatePairedDevice address, agent_path, capability
    registry.unexport agent
    registry[result.first]
  end

  #
  # Returns the object path of device for given address. The device object
  # needs to be first created via CreateDevice or CreatePairedDevice.
  #
  # @param address [String]
  # @return [Device]
  #
  def find_device(address)
    registry[adapter.FindDevice(address).first]
  end

  #
  # Returns all properties for the adapter. See the properties section for
  # available properties.
  #
  # @return [Hash{String => Object}]
  #
  def get_adapter_properties
    adapter.GetProperties.first
  end

  #
  # This registers the adapter wide agent.
  #
  # The object path defines the path of the agent that will be called when user
  # input is needed.
  #
  # If an application disconnects from the registry all of its registered
  # agents will be removed.
  #
  # The capability parameter can have the values "DisplayOnly", "DisplayYesNo",
  # "KeyboardOnly" and "NoInputNoOutput" which reflects the input and output
  # capabilities of the agent. If an empty string is used it will fallback to
  # "DisplayYesNo".
  #
  # @param agent [Agent]
  # @param capability [String]
  # @return [void]
  #
  def register_agent(agent, capability)
    adapter.RegisterAgent registry.export(agent), capability
  end

  #
  # Release a previously requested session. It sets adapter to the mode in use
  # on the moment of session request.
  #
  # SetProperty method call changes adapter's mode persistently, such that
  # session release will not modify it.
  #
  # @return [void]
  #
  def release_session
    adapter.ReleaseSession
  end

  #
  # This removes the remote device object at the given path. It will remove
  # also the pairing information.
  #
  # @param device [Device]
  # @return [void]
  #
  def remove_device(device)
    adapter.RemoveDevice device.path
  end

  #
  # This method requests a client session that provides operational Bluetooth.
  # A possible mode change must be confirmed by the user via the agent.
  #
  # Clients may request multiple sessions. All sessions are released when
  # adapter's mode is changed to off state.
  #
  # @return [void]
  #
  def request_session
    adapter.RequestSession
  end

  #
  # Changes the value of the specified property. Only properties that are
  # listed a read-write are changeable. On success this will emit a
  # PropertyChanged signal.
  #
  # @param name [String]
  # @param value [Object]
  # @return [void]
  #
  def set_adapter_property(name, value)
    adapter.SetProperty name, value
  end

  #
  # This method starts the device discovery session. This includes an inquiry
  # procedure and remote device name resolving. Use StopDiscovery to release
  # the sessions acquired.
  #
  # This process will start emitting DeviceFound and PropertyChanged
  # "Discovering" signals.
  #
  # @return [void]
  #
  def start_discovery
    adapter.StartDiscovery
  end

  #
  # This method will cancel any previous StartDiscovery transaction.
  #
  # Note that a discovery procedure is shared between all discovery sessions
  # thus calling StopDiscovery will only release a single session.
  #
  # @return [void]
  #
  def stop_discovery
    adapter.StopDiscovery
  end

  #
  # This unregisters the agent that has been previously registered. The object
  # path parameter must match the same value that has been used on
  # registration.
  #
  # @return [void]
  #
  def unregister_agent(agent)
    adapter.UnregisterAgent registry.export(agent)
    registry.unexport agent
  end
end
