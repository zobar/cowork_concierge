# coding: utf-8

# :nodoc:
module BlueZ::Manager
  include BlueZ::Interface

  def self.notification(registry, member, args)
    case member
    when 'AdapterAdded'
      yield 'adapter_added', registry[args[0]]
    when 'AdapterRemoved'
      yield 'adapter_removed', registry[args[0]]
    when 'DefaultAdapterChanged'
      yield 'default_adapter_changed', registry[args[0]]
    when 'PropertyChanged'
      yield 'manager_property_changed', *args
    end
  end

  #
  # @!attribute [r] adapters
  #
  # List of adapter object paths.
  #
  # @return [Array<Adapter>]
  #
  def adapters
    get_manager_properties['Adapters'].map(&registry.method(:[]))
  end

  def manager
    @manager ||= @proxy['org.bluez.Manager']
  end

  #
  # Returns object path for the default adapter.
  #
  # @return [Adapter]
  #
  def default_adapter
    registry[manager.DefaultAdapter.first]
  end

  #
  # Returns object path for the specified adapter. Valid patterns are "hci0"
  # or "00:11:22:33:44:55".
  #
  # @param pattern [String]
  # @return [Adapter]
  #
  def find_adapter(pattern)
    registry[manager.FindAdapter(pattern).first]
  end

  #
  # Returns all global properties. See the properties section for available
  # properties.
  #
  # @return [Hash{String => Object}]
  #
  def get_manager_properties
    manager.GetProperties.first
  end
end
