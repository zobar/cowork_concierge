# coding: utf-8

require 'dbus'

# :nodoc:
class BlueZ::Agent
  autoload :Interface, 'bluez/agent/interface'

  #
  # This method gets called when the service daemon needs to authorize a
  # connection/service request.
  #
  # @param device [Device]
  # @param uuid [String]
  # @return [void]
  #
  def authorize(device, uuid)
  end

  #
  # This method gets called to indicate that the agent request failed before a
  # reply was returned.
  #
  # @return [void]
  #
  def cancel
  end

  #
  # This method gets called if a mode change is requested that needs to be
  # confirmed by the user. An example would be leaving flight mode.
  #
  # @param mode [String]
  # @return [void]
  #
  def confirm_mode_change(mode)
  end

  #
  # This method gets called when the service daemon needs to display a passkey
  # for an authentication.
  #
  # The entered parameter indicates the number of already typed keys on the
  # remote side.
  #
  # An empty reply should be returned. When the passkey needs no longer to be
  # displayed, the Cancel method of the agent will be called.
  #
  # During the pairing process this method might be called multiple times to
  # update the entered value.
  #
  # Note that the passkey will always be a 6-digit number, so the display
  # should be zero-padded at the start if the value contains less than 6
  # digits.
  #
  # @param device [Device]
  # @param passkey [Fixnum]
  # @param entered [Fixnum]
  # @return [void]
  #
  def display_passkey(device, passkey, entered)
  end

  def interface(bus)
    @interface ||= Interface.new bus, self
  end

  #
  # This method gets called when the service daemon unregisters the agent. An
  # agent can use it to do cleanup tasks. There is no need to unregister the
  # agent, because when this method gets called it has already been
  # unregistered.
  #
  # @return [void]
  #
  def release
  end

  #
  # This method gets called when the service daemon needs to confirm a passkey
  # for an authentication.
  #
  # To confirm the value it should return an empty reply or an error in case
  # the passkey is invalid.
  #
  # Note that the passkey will always be a 6-digit number, so the display
  # should be zero-padded at the start if the value contains less than 6
  # digits.
  #
  # @param device [Device]
  # @param passkey [Fixnum]
  # @return [void]
  #
  def request_confirmation(device, passkey)
  end

  #
  # This method gets called when the service daemon needs to get the passkey
  # for an authentication.
  #
  # The return value should be a numeric value between 0-999999.
  #
  # @param device [Device]
  # @return [Fixnum]
  #
  def request_passkey(device)
  end

  #
  # This method gets called when the service daemon needs to get the passkey
  # for an authentication.
  #
  # The return value should be a string of 1-16 characters length. The string
  # can be alphanumeric.
  #
  # @param device [Device]
  # @return [String]
  #
  def request_pin_code(device)
  end
end
