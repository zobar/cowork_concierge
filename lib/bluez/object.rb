# coding: utf-8

require 'celluloid'

module BlueZ
  # :nodoc:
  class Object
    extend Forwardable
    include Celluloid::Notifications

    attr_reader :proxy, :registry

    def inspect
      interfaces = singleton_class.included_modules
        .select { |mod| mod < Interface }
        .map(&:name)
        .sort
      "#<#{self.class} (#{interfaces.join(', ')}) #{path.inspect}>"
    end

    private

    def_delegators :proxy, :path

    def initialize(registry, proxy, interfaces)
      @proxy = proxy
      @registry = registry
      interfaces.each(&method(:extend))
    end
  end
end
