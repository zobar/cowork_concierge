# coding: utf-8

require 'celluloid'

#
# Interface to BlueZ, the official Linux Bluetooth protocol stack.
#
module BlueZ
  AF_BLUETOOTH   = 31
  BTPROTO_L2CAP  = 0
  L2CAP_ECHO_REQ = 0x08

  PF_BLUETOOTH   = AF_BLUETOOTH

  autoload :Adapter,    'bluez/adapter'
  autoload :Agent,      'bluez/agent'
  autoload :Device,     'bluez/device'
  autoload :Interface,  'bluez/interface'
  autoload :Manager,    'bluez/manager'
  autoload :Object,     'bluez/object'
  autoload :Registry,   'bluez/registry'

  class << self
    def manager
      @manager ||= registry['/']
    end

    def registry
      @registry ||= Registry.new
    end
  end
end
