# coding: utf-8

require 'dbus'

module CelluloidDBus
  # :nodoc:
  class SystemBus < DBus::ASystemBus
    include Connection
  end
end
