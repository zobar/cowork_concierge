# coding: utf-8

require 'celluloid/io'
require 'dbus'

# :nodoc:
module CelluloidDBus::Connection
  class << self
    private

    def included(other)
      other.send :include, Celluloid::IO
      other.send :include, Celluloid::Notifications
    end
  end

  def add_match(mr, &slot)
    fail NotImplementedError, "Subscribe to 'dbus_signal' instead."
  end

  def remove_match(mr)
    fail NotImplementedError, "Unsubscribe from 'dbus_signal' instead."
  end

  def service(name)
    DBus::Service.new name, Celluloid::Actor.current
  end

  private

  def connect_to_unix(params)
    path = params['path']
    @socket = Celluloid::IO::UNIXSocket.new path
    init_connection
  end

  def initialize
    async.run
    super
    proxy.AddMatch "type='signal'"
  end

  def process(m)
    return if m.nil?
    if m.message_type == DBus::Message::SIGNAL
      publish 'dbus_signal', m.path, m.interface, m.member, *m.params
    else
      super
    end
  end

  def run
    loop do
      update_buffer
      while (message = pop_message)
        # rubocop:disable FavorModifier
        process message
      end
    end
  end

  def send_sync(m)
    return if m.nil?
    unblock = Celluloid::Condition.new
    on_return m do |rmsg|
      yield rmsg
      unblock.signal
    end
    send m.marshall
    unblock.wait
  end

  def update_buffer
    @buffer += @socket.sysread DBus::Connection::MSG_BUF_SIZE
  end
end
