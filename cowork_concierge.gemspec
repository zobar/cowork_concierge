# coding: utf-8

$:.unshift File.join(File.dirname(__FILE__), 'lib')

require 'cowork_concierge/version'

def CoworkConcierge.gemspec
  @gemspec ||= Gem::Specification.new do |s|
    s.authors     = ['David P Kleinschmidt']
    s.description = "Opens the damn door. It's better because it's more complicated."
    s.email       = 'david@kleinschmidt.name'
    s.homepage    = 'http://github.com/zobar/cowork-concierge'
    s.license     = 'MIT'
    s.name        = 'cowork_concierge'
    s.summary     = 'Opens the damn door.'
    s.version     = CoworkConcierge::VERSION

    s.executables = ['cowork_concierge']
    s.files       = Dir['LICENSE.txt', 'lib/**/*.rb'].sort

    s.add_dependency 'celluloid-io'
    s.add_dependency 'ruby-dbus'

    s.add_development_dependency 'rake'
    s.add_development_dependency 'redcarpet'
    s.add_development_dependency 'rubocop'
    s.add_development_dependency 'yard'
  end
end

CoworkConcierge.gemspec
